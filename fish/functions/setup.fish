## Defines abbreviations
function setup --description 'defines abbreviations'
  abbr gg go get GITHUB_URL
  abbr gs git status
  abbr gd git diff
  abbr gc git cm
  abbr ga git aa
  abbr gp git push
  abbr gco git co
  abbr gpl git pull
  abbr gma git aa; and git cm
  abbr gb git b -a
  abbr binfo brew info
  abbr bi brew install
  abbr brews brew leaves
  abbr cl clear
  abbr del trash
  abbr sdel rm -rf
  abbr rubys rbenv install -l
  abbr gems gem query --local
  abbr gemup gem update --system
  abbr nig npm install -g
  abbr npms npm list -g --depth=0
  abbr op open .
  abbr opa open -a APP_NAME
  abbr rc runcloud
  abbr symlink ln -s
  abbr tkill tmux kill-session -t
  abbr suckit wget -mkEpnp url_here
end
