# Source Colors
source ~/.config/fish/customs/_colors.fish
# Source Aliasess
source ~/.config/fish/customs/_aliases.fish
# Source Exports
source ~/.config/fish/customs/_exports.fish

# Source Exports
source /usr/local/etc/grc.fish # https://github.com/oh-my-fish/plugin-grc/issues/20#issue-296031557

# Source Multi-function files
source ~/.config/fish/customs/git_workflow.fish
source ~/.config/fish/customs/devops_workflow.fish
source ~/.config/fish/customs/open_workflow.fish
source ~/.config/fish/customs/repos_workflow.fish
